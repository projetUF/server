import db from "../config/database.js";

export const getFavories = (result) => {
    db.query(`SELECT * FROM favories`, (err, results) => {
        if (err) {
            console.error(`ERREUR getFavories : ${err}`);
            result(err, null);
        } else
            result(null, results);
    })
}

export const getFavoryByUserId = (id, result) => {
    db.query(`SELECT * FROM favories WHERE user_id = ${id}`, (err, results) => {
        if (err) {
            console.error(`ERREUR getFavoryByUserId : ${err}`);
            result(err, null);
        } else
            result(null, results);
    })
}

export const getFavoryByDefiId = (id, result) => {
    db.query(`SELECT * FROM favories WHERE defi_id = ${id}`, (err, results) => {
        if (err) {
            console.error(`ERREUR getFavoryByDefiId : ${err}`);
            result(err, null);
        } else
            result(null, results);
    })
}

export const insertFavory = (data, result) => {
    db.query(`INSERT INTO favories SET defi_id = ${data.defi_id}, user_id = ${data.user_id}`, (err, results) => {
        if (err) {
            console.error(`ERREUR insertFavory : ${err}`);
            result(err, null);
        } else
            result(null, results);
    })
}

export const deleteFavoryById = (data, result) => {
    db.query(`DELETE FROM favories WHERE user_id = ${data.user_id} AND defi_id = ${data.defi_id}`, (err, results) => {
        if (err) {
            console.error(`ERREUR deleteFavoryById : ${err}`);
            result(err, null);
        } else
            result(null, results);
    })
}