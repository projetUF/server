import db from "../config/database.js";

export const getNews = (result) => {
    db.query(`SELECT * FROM news`, (err, results) => {
        if (err) {
            console.error(`ERREUR getNews : ${err}`);
            result(err, null);
        } else
            result(null, results);
    })
}

export const getNewById = (id, result) => {
    db.query(`SELECT * FROM news WHERE id = ${id}`, (err, results) => {
        if (err) {
            console.error(`ERREUR getNewById : ${err}`);
            result(err, null);
        } else
            result(null, results);
    })
}

export const insertNew = (data, result) => {
    db.query(`INSERT INTO news (title, description, appfile_id, creation_date, modif_date) VALUES ('${data.title}', '${data.description}', ${data.appfile_id}, '${data.creation_date}', '${data.modif_date}')`, (err, results) => {
        if (err) {
            console.error(`ERREUR insertNew : ${err}`);
            result(err, null);
        } else
            result(null, results);
    })
}

export const updateNewById = (data, result) => {
    db.query(`UPDATE news SET title = ${data.title}, description = ${data.description}, appfiles_id = ${data.appfiles_id}, modif_date = ${data.modif_date} WHERE id = ${data.id}`, (err, results) => {
        if (err) {
            console.error(`ERREUR updateNewById : ${err}`);
            result(err, null);
        } else
            result(null, results);
    })
}

export const deleteNewById = (id, result) => {
    db.query(`DELETE FROM news WHERE id = ${id}`, (err, results) => {
        if (err) {
            console.error(`ERREUR deleteNewById : ${err}`);
            result(err, null);
        } else
            result(null, results);
    })
}