import db from "../config/database.js";

export const getAllSessionUser = (result) => {
    db.query(`SELECT * FROM sessions_users`, (err, results) => {
        if (err) {
            console.error(`ERREUR getAllSessionUser : ${err}`);
            result(err, null);
        } else
            result(null, results);
    })
}

export const getSessionUserBySessionId = (id, result) => {
    db.query(`SELECT * FROM sessions_users WHERE session_id = ${id}`, (err, results) => {
        if (err) {
            console.error(`ERREUR getSessionUserBySessionId : ${err}`);
            result(err, null);
        } else
            result(null, results);
    })
}

export const getSessionUserByUserId = (id, result) => {
    db.query(`SELECT * FROM sessions_users WHERE user_id = ${id}`, (err, results) => {
        if (err) {
            console.error(`ERREUR getSessionUserByUserId : ${err}`);
            result(err, null);
        } else
            result(null, results);
    })
}

export const insertSessionUser = (data, result) => {
    db.query(`INSERT INTO sessions_users SET session_id = ${data.session_id}, user_id = ${data.user_id}`, (err, results) => {
        if (err) {
            console.error(`ERREUR insertSessionUser : ${err}`);
            result(err, null);
        } else
            result(null, results);
    })
}

export const deleteSessionUserById = (data, result) => {
    db.query(`DELETE FROM sessions_users WHERE user_id = ${data.user_id} AND session_id = ${data.session_id}`, (err, results) => {
        if (err) {
            console.error(`ERREUR deleteSessionUserById : ${err}`);
            result(err, null);
        } else
            result(null, results);
    })
}