import db from "../config/database.js";

export const getAllDefiUser = (result) => {
    db.query(`SELECT * FROM defis_users`, (err, results) => {
        if (err) {
            console.error(`ERREUR getAllDefiUser : ${err}`);
            result(err, null);
        } else
            result(null, results);
    })
}

export const getDefiUserByDefiId = (id, result) => {
    db.query(`SELECT * FROM defis_users WHERE defi_id = ${id}`, (err, results) => {
        if (err) {
            console.error(`ERREUR getDefiUserByDefiId : ${err}`);
            result(err, null);
        } else
            result(null, results);
    })
}

export const getDefiUserByUserId = (id, result) => {
    db.query(`SELECT * FROM defis_users WHERE user_id = ${id}`, (err, results) => {
        if (err) {
            console.error(`ERREUR getDefiUserByUserId : ${err}`);
            result(err, null);
        } else
            result(null, results);
    })
}

export const insertDefiUser = (data, result) => {
    db.query(`INSERT INTO defis_users SET defi_id = ${data.defi_id}, user_id = ${data.user_id}, state = 'submit', appfile_id = ${data.appfile_id}`, (err, results) => {
        if (err) {
            console.error(`ERREUR insertDefiUser : ${err}`);
            result(err, null);
        } else
            result(null, results);
    })
}

export const updateDefiUserById = (data, result) => {
    db.query(`UPDATE defis_users SET state = ${data.state}, comment = ${data.comment}, appfile_id = ${data.appfile_id}, modif_date = NOW() WHERE user_id = ${data.user_id} AND defi_id = ${data.defi_id}`, (err, results) => {
        if (err) {
            console.error(`ERREUR updateDefiUserById : ${err}`);
            result(err, null);
        } else
            result(null, results);
    })
}

export const deleteDefiUserById = (data, result) => {
    db.query(`DELETE FROM defis_users WHERE user_id = ${data.user_id} AND defi_id = ${data.defi_id}`, (err, results) => {
        if (err) {
            console.error(`ERREUR deleteDefiUserById : ${err}`);
            result(err, null);
        } else
            result(null, results);
    })
}