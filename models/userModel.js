import db from "../config/database.js";

export const getUsers = (result) => {
    db.query(`SELECT * FROM users`, (err, results) => {
        if (err) {
            console.error(`ERREUR getUsers : ${err}`);
            result(err, null);
        } else
            result(null, results);
    })
}

export const getRankingUsers = (result) => {
    db.query(`SELECT username, score FROM users ORDER BY score DESC LIMIT 10`, (err, results) => {
        if (err) {
            console.error(`ERREUR getRankingUsers : ${err}`);
            result(err, null);
        } else
            result(null, results);
    })
}

export const getUserById = (id, result) => {
    db.query(`SELECT * FROM users WHERE id = ${id}`, (err, results) => {
        if (err) {
            console.error(`ERREUR getUserById : ${err}`);
            result(err, null);
        } else
            result(null, results);
    })
}

export const insertUser = (data, result) => {
    db.query(`INSERT INTO users (name, firstname, username, mail, password, date_of_birth, score, phone, address, postal_code, city, country, role_id) VALUES ("${data.name}", "${data.firstname}", "${data.username}", "${data.mail}", "${data.password}", "${data.date_of_birth}", ${data.score}, "${data.phone}", "${data.address}", "${data.postal_code}", "${data.city}", "${data.country}", ${data.role_id})`, (err, results) => {
        if (err) {
            console.error(`ERREUR insertUser : ${err}`);
            result(err, null);
        } else
            result(null, results);
    })
}

export const updateUserById = (data, result) => {
    db.query(`UPDATE users SET name = "${data.name}", firstname = "${data.firstname}", username = "${data.username}", mail = "${data.mail}", password = "${data.password}", date_of_birth = "${data.date_of_birth}", score = "${data.score}", phone = "${data.phone}", address = "${data.address}", postal_code = "${data.postal_code}", city = "${data.city}", country = "${data.country}" WHERE id = ${data.id}`, (err, results) => {
        if (err) {
            console.error(`ERREUR updateUserById : ${err}`);
            result(err, null);
        } else
            result(null, results);
    })
}

export const deleteUserById = (id, result) => {
    db.query(`DELETE FROM users WHERE id = ${id}`, (err, results) => {
        if (err) {
            console.error(`ERREUR deleteUserById : ${err}`);
            result(err, null);
        } else
            result(null, results);
    })
}