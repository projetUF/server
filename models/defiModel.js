import db from "../config/database.js";

export const getDefis = (result) => {
    db.query(`SELECT * FROM defis`, (err, results) => {
        if (err) {
            console.error(`ERREUR getDefis : ${err}`);
            result(err, null);
        } else
            result(null, results);
    })
}

export const getDefiById = (id, result) => {
    db.query(`SELECT * FROM defis WHERE id = ${id}`, (err, results) => {
        if (err) {
            console.error(`ERREUR getDefiById : ${err}`);
            result(err, null);
        } else
            result(null, results);
    })
}

export const insertDefi = (data, result) => {
    db.query(`INSERT INTO defis SET ${data}`, (err, results) => {
        if (err) {
            console.error(`ERREUR insertDefi : ${err}`);
            result(err, null);
        } else
            result(null, results);
    })
}

export const updateDefiById = (data, result) => {
    db.query(`UPDATE defis SET title = ${data.title}, description = ${data.description}, points = ${data.points}, appfiles_id = ${data.appfiles_id}, modif_date = ${modif_date} WHERE id = ${data.id}`, (err, results) => {
        if (err) {
            console.error(`ERREUR updateDefiById : ${err}`);
            result(err, null);
        } else
            result(null, results);
    })
}

export const deleteDefiById = (id, result) => {
    db.query(`DELETE FROM defis WHERE id = ${id}`, (err, results) => {
        if (err) {
            console.error(`ERREUR deleteDefiById : ${err}`);
            result(err, null);
        } else
            result(null, results);
    })
}