import db from "../config/database.js";

export const getSessions = (result) => {
    db.query(`SELECT * FROM sessions`, (err, results) => {
        if (err) {
            console.error(`ERREUR getSessions : ${err}`);
            result(err, null);
        } else
            result(null, results);
    })
}

export const getSessionById = (id, result) => {
    db.query(`SELECT * FROM sessions WHERE id = ${id}`, (err, results) => {
        if (err) {
            console.error(`ERREUR getSessionById : ${err}`);
            result(err, null);
        } else
            result(null, results);
    })
}

export const getActiveSession = (result) => {
    db.query(`SELECT * FROM sessions WHERE active = 1`, (err, results) => {
        if (err) {
            console.error(`ERREUR getActiveSession : ${err}`);
            result(err, null);
        } else
            result(null, results);
    })
}

export const insertSession = (data, result) => {
    db.query(`INSERT INTO sessions SET date_of_begin = '${data.date_of_begin}', date_of_end = '${data.date_of_end}', active = ${data.active}`, (err, results) => {
        if (err) {
            console.error(`ERREUR insertSession : ${err}`);
            result(err, null);
        } else
            result(null, results);
    })
}

export const updateSessionById = (data, result) => {
    db.query(`UPDATE sessions SET date_of_begin = '${data.date_of_begin}', date_of_end = '${data.date_of_end}', active = ${data.active}, modif_date = NOW() WHERE id = ${data.id}`, (err, results) => {
        if (err) {
            console.error(`ERREUR updateSessionById : ${err}`);
            result(err, null);
        } else
            result(null, results);
    })
}

export const deleteSessionById = (id, result) => {
    db.query(`DELETE FROM sessions WHERE id = ${id}`, (err, results) => {
        if (err) {
            console.error(`ERREUR deleteSessionById : ${err}`);
            result(err, null);
        } else
            result(null, results);
    })
}