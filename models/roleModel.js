import db from "../config/database.js";

export const getRoles = (result) => {
    db.query(`SELECT * FROM roles`, (err, results) => {
        if (err) {
            console.error(`ERREUR getRole : ${err}`);
            result(err, null);
        } else
            result(null, results);
    })
}

export const getRoleById = (id, result) => {
    db.query(`SELECT * FROM roles WHERE id = ${id}`, (err, results) => {
        if (err) {
            console.error(`ERREUR getRoleById : ${err}`);
            result(err, null);
        } else
            result(null, results);
    })
}

export const insertRole = (data, result) => {
    db.query(`INSERT INTO roles SET name = ${data.name}, modif_date = NOW()`, (err, results) => {
        if (err) {
            console.error(`ERREUR insertRole : ${err}`);
            result(err, null);
        } else
            result(null, results);
    })
}

export const updateRoleById = (data, result) => {
    db.query(`UPDATE roles SET name = ${data.name}, modif_date = ${data.modif_date} WHERE id = ${data.id}`, (err, results) => {
        if (err) {
            console.error(`ERREUR updateRoleById : ${err}`);
            result(err, null);
        } else
            result(null, results);
    })
}

export const deleteRoleById = (id, result) => {
    db.query(`DELETE FROM roles WHERE id = ${id}`, (err, results) => {
        if (err) {
            console.error(`ERREUR deleteRoleById : ${err}`);
            result(err, null);
        } else
            result(null, results);
    })
}