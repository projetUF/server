import express from 'express';
import { createDefi, deleteDefi, showDefiById, showDefis, updateDefi } from '../controllers/defi.js';
import { createNew, deleteNew, showNewById, showNews, updateNew } from '../controllers/new.js';
import { createSession, showSessions, showActiveSession, showSessionById, updateSession, deleteSession } from '../controllers/session.js';
import { showRankingUsers, showUsers, showUserById, createUser, updateUser, deleteUser } from "../controllers/user.js";
import { showAllSessionUser, showSessionUserBySessionId, showSessionUserByUserId, createSessionUser, deleteSessionUser } from "../controllers/sessionUser.js";
import { showAllDefiUser, showDefiUserByDefiId, showDefiUserByUserId, createDefiUser, updateDefiUser, deleteDefiUser } from "../controllers/defiUser.js";
import { showFavories, showFavoryByUserId, showFavoryByDefiId, createFavory, deleteFavory } from "../controllers/favory.js";

const router = express.Router();

router.get('/', (req, res) => {
    res.send('API : @ Home A Game | ALLIONE Florian - FABBRI Théo - B2A - 20/21');
});

// Users routes
router.post('/users/create', createUser);
router.get('/users', showUsers);
router.get('/users/rank', showRankingUsers);
router.get('/users/:id', showUserById);
router.put('/users/:id/update', updateUser);
router.delete('/users/:id/delete', deleteUser);

// Sessions routes
router.post('/sessions/create', createSession);
router.get('/sessions', showSessions);
router.get('/sessions/active', showActiveSession);
router.get('/sessions/:id', showSessionById);
router.put('/sessions/:id/update', updateSession);
router.delete('/sessions/:id/delete', deleteSession);

// Defis routes
router.post('/defis/create', createDefi);
router.get('/defis', showDefis);
router.get('/defis/:id', showDefiById);
router.put('/defis/:id/update', updateDefi);
router.delete('/defis/:id/delete', deleteDefi);

// News routes
router.post('/news/create', createNew);
router.get('/news', showNews);
router.get('/news/:id', showNewById);
router.put('/news/:id/update', updateNew);
router.delete('/news/:id/delete', deleteNew);

// Roles routes
router.post('/roles/create', createNew);
router.get('/roles', showNews);
router.get('/roles/:id', showNewById);
router.put('/roles/:id/update', updateNew);
router.delete('/roles/:id/delete', deleteNew);

// SessionUser routes
router.get('/sessions-users', showAllSessionUser);
router.get('/sessions-users/:id/sessions', showSessionUserBySessionId);
router.get('/sessions-users/:id/users', showSessionUserByUserId);
router.post('/sessions-users/create', createSessionUser);
router.delete('/sessions-users/delete', deleteSessionUser);

// DefiUser routes
router.get('/defis-users', showAllDefiUser);
router.get('/defis-users/:id/defis', showDefiUserByDefiId);
router.get('/defis-users/:id/users', showDefiUserByUserId);
router.post('/defis-users/create', createDefiUser);
router.put('/defis-users/update', updateDefiUser);
router.delete('/defis-users/delete', deleteDefiUser);

// SessionUser routes
router.get('/favories', showFavories);
router.get('/favories/:id/users', showFavoryByUserId);
router.get('/favories/:id/defis', showFavoryByDefiId);
router.post('/favories/create', createFavory);
router.delete('/favories/delete', deleteFavory);

export default router;