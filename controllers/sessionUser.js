import { getAllSessionUser, getSessionUserBySessionId, getSessionUserByUserId, insertSessionUser, deleteSessionUserById } from "../models/sessionUserModel.js";

export const showAllSessionUser = (req, res) => {
    getAllSessionUser((err, results) => {
        if (err) {
            res.send(err);
        } else {
            res.json(results);
        }
    });
};

export const showSessionUserBySessionId = (req, res) => {
    getSessionUserBySessionId(req.params.id, (err, results) => {
        if (err) {
            res.send(err);
        } else {
            res.json(results);
        }
    });
};

export const showSessionUserByUserId = (req, res) => {
    getSessionUserByUserId(req.params.id, (err, results) => {
        if (err) {
            res.send(err);
        } else {
            res.json(results);
        }
    });
};

export const createSessionUser = (req, res) => {
    insertSessionUser(req.body, (err, results) => {
        if (err) {
            res.send(err);
        } else {
            res.json(results);
        }
    });
};

export const updateSessionUser = (req, res) => {
    updateSessionUserById(req.body, req.params.id, (err, results) => {
        if (err) {
            res.send(err);
        } else {
            res.json(results);
        }
    });
};

export const deleteSessionUser = (req, res) => {
    deleteSessionUserById(req.body, (err, results) => {
        if (err) {
            res.send(err);
        } else {
            res.json(results);
        }
    });
};