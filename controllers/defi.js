import { getDefis, getDefiById, insertDefi, updateDefiById, deleteDefiById } from "../models/defiModel.js";

export const showDefis = (req, res) => {
    getDefis((err, results) => {
        if (err) {
            res.send(err);
        } else {
            res.json(results);
        }
    });
};

export const showDefiById = (req, res) => {
    getDefiById(req.params.id, (err, results) => {
        if (err) {
            res.send(err);
        } else {
            res.json(results);
        }
    });
};

export const createDefi = (req, res) => {
    insertDefi(req.body, (err, results) => {
        if (err) {
            res.send(err);
        } else {
            res.json(results);
        }
    });
};

export const updateDefi = (req, res) => {
    updateDefiById(req.body, (err, results) => {
        if (err) {
            res.send(err);
        } else {
            res.json(results);
        }
    });
};

export const deleteDefi = (req, res) => {
    deleteDefiById(req.params.id, (err, results) => {
        if (err) {
            res.send(err);
        } else {
            res.json(results);
        }
    });
};

