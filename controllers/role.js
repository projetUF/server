import { getRoles, getRoleById, insertRole, updateRoleById, deleteRoleById } from "../models/roleModel.js";

export const showRoles = (req, res) => {
    getRoles((err, results) => {
        if (err) {
            res.send(err);
        } else {
            res.json(results);
        }
    });
};

export const showRoleById = (req, res) => {
    getRoleById(req.params.id, (err, results) => {
        if (err) {
            res.send(err);
        } else {
            res.json(results);
        }
    });
};

export const createRole = (req, res) => {
    insertRole(req.body, (err, results) => {
        if (err) {
            res.send(err);
        } else {
            res.json(results);
        }
    });
};

export const updateRole = (req, res) => {
    updateRoleById(req.body, (err, results) => {
        if (err) {
            res.send(err);
        } else {
            res.json(results);
        }
    });
};

export const deleteRole = (req, res) => {
    deleteRoleById(req.params.id, (err, results) => {
        if (err) {
            res.send(err);
        } else {
            res.json(results);
        }
    });
};

