import { getAllDefiUser, getDefiUserByDefiId, getDefiUserByUserId, insertDefiUser, updateDefiUserById, deleteDefiUserById } from "../models/DefiUserModel.js";

export const showAllDefiUser = (req, res) => {
    getAllDefiUser((err, results) => {
        if (err) {
            res.send(err);
        } else {
            res.json(results);
        }
    });
};

export const showDefiUserByDefiId = (req, res) => {
    getDefiUserByDefiId(req.params.id, (err, results) => {
        if (err) {
            res.send(err);
        } else {
            res.json(results);
        }
    });
};

export const showDefiUserByUserId = (req, res) => {
    getDefiUserByUserId(req.params.id, (err, results) => {
        if (err) {
            res.send(err);
        } else {
            res.json(results);
        }
    });
};

export const createDefiUser = (req, res) => {
    insertDefiUser(req.body, (err, results) => {
        if (err) {
            res.send(err);
        } else {
            res.json(results);
        }
    });
};

export const updateDefiUser = (req, res) => {
    updateDefiUserById(req.body, req.params.id, (err, results) => {
        if (err) {
            res.send(err);
        } else {
            res.json(results);
        }
    });
};

export const deleteDefiUser = (req, res) => {
    deleteDefiUserById(req.body, (err, results) => {
        if (err) {
            res.send(err);
        } else {
            res.json(results);
        }
    });
};