import { getFavories, getFavoryByUserId, getFavoryByDefiId, insertFavory, deleteFavoryById } from "../models/favoryModel.js";

export const showFavories = (req, res) => {
    getFavories((err, results) => {
        if (err) {
            res.send(err);
        } else {
            res.json(results);
        }
    });
};

export const showFavoryByUserId = (req, res) => {
    getFavoryByUserId(req.params.id, (err, results) => {
        if (err) {
            res.send(err);
        } else {
            res.json(results);
        }
    });
};

export const showFavoryByDefiId = (req, res) => {
    getFavoryByDefiId(req.params.id, (err, results) => {
        if (err) {
            res.send(err);
        } else {
            res.json(results);
        }
    });
};

export const createFavory = (req, res) => {
    insertFavory(req.body, (err, results) => {
        if (err) {
            res.send(err);
        } else {
            res.json(results);
        }
    });
};

export const deleteFavory = (req, res) => {
    deleteFavoryById(req.body, (err, results) => {
        if (err) {
            res.send(err);
        } else {
            res.json(results);
        }
    });
};

