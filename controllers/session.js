import { getSessions, getSessionById, getActiveSession, insertSession, updateSessionById, deleteSessionById } from "../models/sessionModel.js";

export const showSessions = (req, res) => {
    getSessions((err, results) => {
        if (err) {
            res.send(err);
        } else {
            res.json(results);
        }
    });
};

export const showSessionById = (req, res) => {
    getSessionById(req.params.id, (err, results) => {
        if (err) {
            res.send(err);
        } else {
            res.json(results);
        }
    });
};

export const showActiveSession = (req, res) => {
    getActiveSession((err, results) => {
        if (err) {
            res.send(err);
        } else {
            res.json(results);
        }
    });
};

export const createSession = (req, res) => {
    insertSession(req.body, (err, results) => {
        if (err) {
            res.send(err);
        } else {
            res.json(results);
        }
    });
};

export const updateSession = (req, res) => {
    updateSessionById(req.body, (err, results) => {
        if (err) {
            res.send(err);
        } else {
            res.json(results);
        }
    });
};

export const deleteSession = (req, res) => {
    deleteSessionById(req.params.id, (err, results) => {
        if (err) {
            res.send(err);
        } else {
            res.json(results);
        }
    });
};

