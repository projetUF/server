import { getNews, getNewById, insertNew, updateNewById, deleteNewById } from "../models/newModel.js";

export const showNews = (req, res) => {
    getNews((err, results) => {
        if (err) {
            res.send(err);
        } else {
            res.json(results);
        }
    });
};

export const showNewById = (req, res) => {
    getNewById(req.params.id, (err, results) => {
        if (err) {
            res.send(err);
        } else {
            res.json(results);
        }
    });
};

export const createNew = (req, res) => {
    insertNew(req.body, (err, results) => {
        console.log(results, err);
        if (err) {
            res.send(err);
        } else {

            res.json(results);
        }
    });
};

export const updateNew = (req, res) => {
    updateNewById(req.body, (err, results) => {
        if (err) {
            res.send(err);
        } else {
            res.json(results);
        }
    });
};

export const deleteNew = (req, res) => {
    deleteNewById(req.params.id, (err, results) => {
        if (err) {
            res.send(err);
        } else {
            res.json(results);
        }
    });
};

